<?php

use App\Components\DB\Repository;
use App\Components\DB\DBSimulator;
use App\Models\NodeRepository;
use App\Models\NodeModel;
use PHPUnit\Framework\TestCase;

class RepositoryTest extends TestCase
{
    /**
     * @var Repository
     */
    private $nodeRepository;

    public function setUp()
    {
        $file = __DIR__ . '/data/db2.json';
        file_put_contents($file, json_encode([
            1 => [
                'id' => 1,
                'value' => 'one',
            ],
        ]));
        $db = DBSimulator::getInstance(__DIR__ . '/data/db2.json');
        $this->nodeRepository = new NodeRepository($db);
    }

    public function tearDown()
    {
        DBSimulator::destruct();
    }

    public function testLoadShouldReturnModel()
    {
        $model = $this->nodeRepository->loadOne(1);
        self::assertInstanceOf(NodeModel::class, $model);
        self::assertEquals('one', $model->value);
    }

    public function testLoadAll()
    {
        $collection = $this->nodeRepository->loadAll();
        foreach ($collection as $model) {
            self::assertInstanceOf(NodeModel::class, $model);
            self::assertFalse($model->isNew());
        }
    }

    public function testSaveMethodNewModel()
    {
        $model = new NodeModel();
        $model->value = 'two';
        self::assertNull($model->id);
        self::assertTrue($model->isNew());
        $this->nodeRepository->save($model);
        self::assertNotNull($model->id);
        self::assertFalse($model->isNew());
    }

    public function testSaveMethodNotNewModel()
    {
        $model = $this->nodeRepository->loadOne(1);
        self::assertEquals(1, $model->id);
        self::assertEquals('one', $model->value);
        $model->value = 'two';
        $this->nodeRepository->save($model);
        $model = $this->nodeRepository->loadOne(1);
        self::assertEquals(1, $model->id);
        self::assertEquals('two', $model->value);
    }

    public function testDelete()
    {
        $model = $this->nodeRepository->loadOne(1);
        $this->nodeRepository->delete($model);
        $this->expectException(InvalidArgumentException::class);
        $this->nodeRepository->loadOne(1);
    }
}
