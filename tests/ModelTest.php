<?php

use PHPUnit\Framework\TestCase;
use App\Components\Db\Exceptions\UnknownPropertyException;
use App\Models\NodeModel;

class ModelTest extends TestCase
{
    public function testCreateModel()
    {
        $model = new NodeModel();
        self::assertTrue($model->isNew());
    }

    public function testModelSettersAndGetters()
    {
        $model = new NodeModel();
        $model->value = 'test';
        self::assertEquals('test', $model->value);
    }

    public function testSetterShouldThrowException()
    {
        $model = new NodeModel();
        $this->expectException(UnknownPropertyException::class);
        $model->someField = 'test';
    }

    public function testGetterShouldThrownException()
    {
        $model = new NodeModel();
        $this->expectException(UnknownPropertyException::class);
        $value = $model->wrongField;
    }

    public function testTouched()
    {
        $model = new NodeModel(['value' => 'value'], false);
        self::assertCount(0, $model->getTouched());
        $model->value = 'new_value';
        $touched = $model->getTouched();
        self::assertCount(1, $touched);
        self::assertTrue(in_array('value', $touched));
    }

    public function testResetTouched()
    {
        $model = new NodeModel(['value' => 'value'], false);
        self::assertCount(0, $model->getTouched());
        $model->value = 'new_value';
        self::assertCount(1, $model->getTouched());
        $model->resetTouched();
        self::assertCount(0, $model->getTouched());
    }
}
