<?php

use App\Models\NodeRepository;
use App\Components\DB\DBSimulator;
use App\Components\Cache\FileCache;

trait CacheServiceTrait
{
    /**
     * @var \App\Components\DB\DBSimulator
     */
    private $db;
    /**
     * @var NodeRepository
     */
    private $nodeRepository;
    /**
     * @var \App\Components\Cache\FileCache
     */
    private $cache;

    public function setUp()
    {
        $file = __DIR__ . '/../runtime/db.json';
        copy(__DIR__ . '/../data/db_issue1.json', $file);
        $this->db = DBSimulator::getInstance($file);
        $this->nodeRepository = new NodeRepository($this->db);
        $this->cache = new FileCache(__DIR__ . '/../runtime/cache');
    }

    public function tearDown()
    {
        DBSimulator::destruct();
        unlink(__DIR__ . '/../runtime/cache');
    }
}