<?php

use App\Services\CacheService;
use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/CacheServiceTrait.php';

class Issue1Test extends TestCase
{
    use CacheServiceTrait;

    /**
     * Описание ошибки:
     * 1) Загрузить ноду 29
     * 2) Добавить к ноде 29 ноду "1"
     * 3) Добавить к ноде "1" ноду "11"
     * 4) Загрузить ноду 28
     * 5) Загрузить ноду 1
     * 6) Добавить к ноде 28 ноду "2"
     * 7) Удалить ноду 1
     * 8) Добавить к ноде "2" ноду "22"
     * 9) Добавить к ноде "11" ноду "111"
     * 10) Добавить к ноде "22" ноду "222"
     * 11) Применить
     * Ошибка - в базе нода "22" является ребёнком ноды "111", хотя должна быть ребёнком ноды "2"
     */
    public function testIssueVerify()
    {
        $cacheService = new CacheService($this->cache, $this->db);
        //1
        $cacheService->loadNode(29);
        //2
        $node1 = $cacheService->addNewNode('1', 29);
        //3
        $node11 = $cacheService->addNewNode('11', $node1);
        //4
        $cacheService->loadNode(28);
        //5
        $cacheService->loadNode(1);
        //6
        $node2 = $cacheService->addNewNode('2', 28);
        //7
        $cacheService->deleteNode(1);
        //8
        $node22 = $cacheService->addNewNode('22', $node2);
        //9
        $node111 = $cacheService->addNewNode('111', $node11);
        //10
        $cacheService->addNewNode('222', $node22);
        //11
        $cacheService->saveAll();

        //Reload from db.
        $node111 = $this->nodeRepository->loadOne($node111);
        $node22 = $this->nodeRepository->loadOne($node22);

        //correct behavior
        self::assertNotEquals($node111->getId(), $node22->getParentId());
        self::assertEquals($node2, $node22->getParentId());
    }
}
