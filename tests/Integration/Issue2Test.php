<?php

use App\Services\CacheService;
use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/CacheServiceTrait.php';

class Issue2Test extends TestCase
{
    use CacheServiceTrait;

    /**
     * Описание ошибки:
     * 1) Загрузить ноду 17
     * 2) Загрузить ноду 1
     * 3) Удалить ноду 1
     * 4) Применить
     *
     * Ошибка - После этого в базе нода 17 помечена как удалённая, но в кэше она не удалена, и
     * потому её можно продолжать редактировать.
     */
    public function testIssueVerify()
    {
        $cacheService = new CacheService($this->cache, $this->db);
        $cacheService->loadNode(17)
            ->loadNode(1)
            ->deleteNode(1)
            ->saveAll();

        $node17 = $cacheService->getNode(17);
        self::assertNotNull($node17);
        self::assertTrue($node17->isDeleted);
    }
}
