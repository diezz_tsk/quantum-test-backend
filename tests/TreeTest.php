<?php

use App\Components\Tree;
use App\Models\NodeModel;
use App\Components\NodeNotFoundException;
use PHPUnit\Framework\TestCase;

class TreeTest extends TestCase
{
    public function testTree()
    {
        $tree = new Tree();
        self::assertCount(0, $tree);
        $model = new NodeModel([
            'id' => 1,
            'value' => 'node',
            'parentId' => null,
        ]);
        $tree->addNode($model);
        self::assertCount(1, $tree);
        $model = new NodeModel([
            'id' => 2,
            'value' => 'node',
            'parentId' => 3,
        ]);
        $tree->addNode($model);
        self::assertCount(2, $tree);
    }

    public function testAddChildToTree()
    {
        $tree = new Tree();
        $model1 = new NodeModel([
            'id' => 1,
            'value' => 'node',
            'parentId' => null,
            'isDeleted' => false,
        ]);
        $tree->addNode($model1);
        self::assertCount(1, $tree);
        $model2 = new NodeModel([
            'id' => 2,
            'value' => 'node',
            'parentId' => 1,
            'isDeleted' => false,
        ]);
        $tree->addNode($model2);
        self::assertCount(1, $tree);
    }

    public function testAddRoot()
    {
        $tree = new Tree();
        $model1 = new NodeModel([
            'id' => 3,
            'value' => 'node',
            'parentId' => 1,
            'isDeleted' => false,
        ]);
        $tree->addNode($model1);
        self::assertCount(1, $tree);
        $model2 = new NodeModel([
            'id' => 2,
            'value' => 'node',
            'parentId' => 1,
            'isDeleted' => false,
        ]);
        $tree->addNode($model2);
        self::assertCount(2, $tree);
        $root = new NodeModel([
            'id' => 1,
            'value' => 'node',
            'parentId' => null,
            'isDeleted' => false,
        ]);
        $tree->addNode($root);
        self::assertCount(1, $tree);
    }

    public function testDelete()
    {
        $tree = new Tree();
        $model = new NodeModel([
            'id' => 1,
            'value' => 'node',
            'parentId' => null,
            'isDeleted' => false,
        ]);
        $model2 = new NodeModel([
            'id' => 2,
            'value' => 'node',
            'parentId' => 1,
            'isDeleted' => false,
        ]);
        $tree->addNode($model)->addNode($model2);
        $tree->removeNode(1);
        foreach ($tree->asFlatArray() as $node) {
            self::assertTrue($node->isDeleted());
        }
    }

    public function testDeleteShouldThrowException()
    {
        $tree = new Tree();
        $this->expectException(NodeNotFoundException::class);
        $tree->removeNode(1);
    }
}
