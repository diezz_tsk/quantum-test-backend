<?php

use App\Components\DB\DBSimulator;
use PHPUnit\Framework\TestCase;

class DBSimulatorTest extends TestCase
{
    /**
     * @var DBSimulator
     */
    private $db;

    public function setUp()
    {
        $dbFile = __DIR__ . '/data/db.json';
        file_put_contents($dbFile, json_encode([]));
        $this->db = DBSimulator::getInstance($dbFile);
    }

    public function tearDown()
    {
        DBSimulator::destruct();
    }

    public function testLoad()
    {
        $data = $this->db->loadAll();
        self::assertCount(0, $data);
    }

    public function testInsertShouldSuccess()
    {
        $id = $this->db->insert([
            'value' => 'value',
        ]);
        self::assertEquals(1, $id);
        self::assertCount(1, $this->db->loadAll());
        $id = $this->db->insert(['value' => 'value2']);
        self::assertCount(2, $this->db->loadAll());
        self::assertEquals(2, $id);
    }

    public function testUpdate()
    {
        $this->db->insert([
            'value' => 'value',
        ]);
        $this->db->update(1, [
            'value' => 'new_value',
        ]);
        $model = $this->db->loadOne(1);
        self::assertEquals('new_value', $model['value']);
    }

    public function testUpdateShouldThrowException()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->db->update(10, [
            'value' => 'new_value',
        ]);
    }

    public function testDelete()
    {
        $this->db->insert([
            'id' => 1,
            'value' => 'value',
        ]);
        $model = $this->db->loadOne(1);
        self::assertEquals('value', $model['value']);
        $this->db->delete(1);
        $this->expectException(InvalidArgumentException::class);
        $this->db->loadOne(1);
    }

    public function testDeleteShouldThrowException()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->db->delete(1);
    }
}
