<?php

use App\Components\Node;
use PHPUnit\Framework\TestCase;
use App\Models\NodeModel;

class NodeTest extends TestCase
{
    public function testNodeShouldBeARoot()
    {
        $node = new Node(new NodeModel([
            'id' => 1,
            'value' => 'value',
            'parentId' => null,
            'isDeleted' => false,
        ]));
        self::assertTrue($node->isLeaf());
        self::assertFalse($node->isDeleted());
    }

    public function testValueGettersAndSetters()
    {
        $tree = $this->getTree();
        self::assertEquals('node 1', $tree->getValue()->getValue());
        $tree->getValue()->value = 'new_value';
        self::assertEquals('new_value', $tree->getValue()->getValue());
    }

    public function testNodeSetter()
    {
        $node1 =  new Node(new NodeModel([
            'id' => 1,
            'value' => 'node 1',
            'parentId' => null,
            'isDeleted' => false,
        ]));
        $children = [
            new Node(new NodeModel([
                'id' => 1,
                'value' => 'node 1',
                'parentId' => null,
                'isDeleted' => false,
            ])),
            new Node(new NodeModel([
                'id' => 2,
                'value' => 'node 2',
                'parentId' => null,
                'isDeleted' => false,
            ])),
            new Node(new NodeModel([
                'id' => 3,
                'value' => 'node 3',
                'parentId' => null,
                'isDeleted' => false,
            ])),
            new Node(new NodeModel([
                'id' => 4,
                'value' => 'node 4',
                'parentId' => null,
                'isDeleted' => false,
            ])),
            new Node(new NodeModel([
                'id' => 5,
                'value' => 'node 5',
                'parentId' => null,
                'isDeleted' => false,
            ])),
            new Node(new NodeModel([
                'id' => 6,
                'value' => 'node 6',
                'parentId' => null,
                'isDeleted' => false,
            ])),
        ];
        $node1->setChildren($children);
        self::assertEquals(6, $node1->getChildrenCount());
    }

    public function testSearchShouldReturnValidNode()
    {
        $tree = $this->getTree();
        $searchResult = $tree->findNode(6);
        self::assertNotNull($searchResult);
        self::assertEquals(6, $searchResult->getId());
    }

    public function testSearchItself()
    {
        $tree = $this->getTree();
        $result = $tree->findNode(1);
        self::assertEquals($tree, $result);
    }

    public function testSearchShouldReturnNull()
    {
        $tree = $this->getTree();
        $result = $tree->findNode(100500);
        self::assertNull($result);
    }

    public function testGetAllChildrenShouldReturnSimpleList()
    {
        $tree = $this->getTree();
        $list = $tree->getAllChildren();
        self::assertCount(5, $list);
    }

    public function testDeleteShouldDeleteAllBranch()
    {
        $node2 = $this->getTree()->findNode(2);
        $node2->delete();
        self::assertTrue($node2->isDeleted());
        foreach ($node2->getAllChildren() as $child) {
            self::assertTrue($child->isDeleted());
        }
    }

    /**
     *          1
     *         /|\
     *        2 3 4
     *       /\
     *      5  6
     * @return Node
     */
    private function getTree(): Node
    {
        $node1 = new Node(new NodeModel([
            'id' => 1,
            'value' => 'node 1',
            'parentId' => null,
            'isDeleted' => false,
        ]));
        $node2 = new Node(new NodeModel([
            'id' => 2,
            'value' => 'node 2',
            'parentId' => null,
            'isDeleted' => false,
        ]));
        $node3 = new Node(new NodeModel([
            'id' => 3,
            'value' => 'node 3',
            'parentId' => null,
            'isDeleted' => false,
        ]));
        $node4 = new Node(new NodeModel([
            'id' => 4,
            'value' => 'node 4',
            'parentId' => null,
            'isDeleted' => false,
        ]));
        $node5 = new Node(new NodeModel([
            'id' => 5,
            'value' => 'node 5',
            'parentId' => null,
            'isDeleted' => false,
        ]));
        $node6 = new Node(new NodeModel([
            'id' => 6,
            'value' => 'node 6',
            'parentId' => null,
            'isDeleted' => false,
        ]));

        $node1->addChild($node2)->addChild($node3)->addChild($node4);
        $node2->addChild($node6)->addChild($node5);
        return $node1;
    }
}
