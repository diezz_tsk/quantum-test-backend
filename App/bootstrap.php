<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Http\Response as Response;
use Slim\Container;
use App\Controllers\CacheController;
use App\Components\Cache\FileCache;
use App\Components\DB\DBSimulator;
use App\Controllers\DbController;

$container = $app->getContainer();

$container['cache'] = function(Container $container) {
    $settings = $container->get('settings');
    if (!array_key_exists('cache', $settings) && !array_key_exists('cacheFile', $settings['cache'])) {
        throw new InvalidArgumentException('You must define cache file in App config');
    }

    return new FileCache($settings['cache']['cacheFile']);
};

$container['db'] = function(Container $container) {
    $settings = $container->get('settings');
    if (!array_key_exists('db', $settings) && !array_key_exists('file', $settings['db'])) {
        throw new InvalidArgumentException('You must define db file in App config');
    }

    $file = $settings['db']['file'];
    if (false === file_exists($file)) {
        file_put_contents($file, json_encode([]));
    }

    return DBSimulator::getInstance($file);
};

$container['CacheController'] = function(Container $container) {
    /** @var \App\Components\Cache\CacheInterface $cache */
    $cache = $container->get('cache');
    $connection = $container->get('db');
    return new CacheController($cache, $connection);
};

$container['DbController'] = function(Container $container) {
    $connection = $container->get('db');
    return new DbController($connection);
};

$app->options('/{routes:.+}', function($request, $response, $args) {
    return $response;
});

/** @var \Slim\App $app */
$app->add(function(Request $request, Response $response, $next) use ($container) {
    $response = $next($request, $response);
    return $response->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});
