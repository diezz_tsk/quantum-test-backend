<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Http\Response as Response;
use Respect\Validation\Validator;
use DavidePastore\Slim\Validation\Validation;

// Setting up middleware and DI container.
require_once __DIR__ . '/bootstrap.php';

$app->get('/', function(Request $request, Response $response) {
    $response->getBody()->write("Hello world!");
    return $response;
});

//Define API routes
$app->group('/api', function() {
    $this->get('/cache', 'CacheController:loadAll');
    $this->post('/cache/reset', 'CacheController:reset');
    $this->post('/cache/load/{id}', 'CacheController:loadOne')->add(new Validation([
        'id' => Validator::numeric()->notBlank(),
    ]));
    $this->put('/cache', 'CacheController:create')->add(new Validation([
        'parentId' => Validator::numeric()->notBlank(),
        'value' => Validator::stringType()->notBlank(),
    ]));
    $this->delete('/cache/{id}', 'CacheController:delete')->add(new Validation([
        'id' => Validator::numeric()->notBlank(),
    ]));
    $this->post('/cache/save', 'CacheController:save');

    $this->get('/db', 'DbController:load');
    $this->post('/db/reset', 'DbController:reset');
});
