<?php

namespace App\Controllers;

use App\Components\Cache\CacheInterface;
use App\Components\DB\Interfaces\DBInterface;
use App\Components\Tree;
use App\Models\NodeModel;
use App\Models\NodeRepository;
use App\Services\CacheService;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class CacheController
 *
 * @package App\Controllers
 */
class CacheController
{
    /**
     * @var CacheInterface
     */
    private $cache;
    /**
     * @var DBInterface
     */
    private $connection;

    /**
     * CacheController constructor.
     *
     * @param CacheInterface $cache
     * @param DBInterface    $connection
     */
    public function __construct(CacheInterface $cache, DBInterface $connection)
    {
        $this->cache = $cache;
        $this->connection = $connection;
    }

    /**
     * Return all nodes from tree as JSON.
     *
     * @param Request  $request
     * @param Response $response
     * @param mixed    $args
     *
     * @throws \RuntimeException
     * @return Response
     */
    public function loadAll(Request $request, Response $response, $args)
    {
        return $response->withJson([
            'data' => $this->getCacheService()
                ->getAll()
                ->toArray(),
        ]);
    }

    /**
     * Reset cache state to default.
     *
     * @param Request  $request
     * @param Response $response
     * @param mixed    $args
     *
     * @throws \RuntimeException
     * @return Response
     */
    public function reset(Request $request, Response $response, $args)
    {
        $data = $this->getCacheService()
            ->reset()
            ->getAll();
        return $response->withJson([
            'data' => $data->toArray(),
        ]);
    }

    /**
     * Load one node from DB and save it into cache.
     *
     * @param Request  $request
     * @param Response $response
     * @param mixed    $args
     *
     * @throws \RuntimeException
     * @return Response
     */
    public function loadOne(Request $request, Response $response, $args)
    {
        if ($request->getAttribute('has_errors')) {
            return $response->withStatus(400)
                ->withJson([
                    'error' => 'There are validations errors',
                    'errors' => $request->getAttribute('errors'),
                ]);
        }

        $nodeId = $request->getAttribute('id');
        try {
            $data = $this->getCacheService()
                ->loadNode($nodeId)
                ->getAll();
        } catch (\InvalidArgumentException $e) {
            return $response->withStatus(400)
                ->withJson([
                    'error' => $e->getMessage(),
                ]);
        }

        return $response->withJson([
            'data' => $data->toArray(),
        ]);
    }

    /**
     * Add new node into cached tree.
     *
     * @param Request  $request
     * @param Response $response
     * @param mixed    $args
     *
     * @throws \RuntimeException
     * @return Response
     */
    public function create(Request $request, Response $response, $args)
    {
        if ($request->getAttribute('has_errors')) {
            return $response->withStatus(400)
                ->withJson([
                    'error' => 'There are validations errors',
                    'errors' => $request->getAttribute('errors'),
                ]);
        }

        ['parentId' => $parentId, 'value' => $value] = $request->getParsedBody();
        $cacheService = $this->getCacheService();
        $cacheService->addNewNode($value, $parentId);

        return $response->withJson([
            'data' => $cacheService->getAll()
                ->toArray(),
        ]);
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param          $args
     *
     * @throws \RuntimeException
     * @return Response
     */
    public function delete(Request $request, Response $response, $args)
    {
        if ($request->getAttribute('has_errors')) {
            return $response->withStatus(400)
                ->withJson([
                    'error' => 'There are validations errors',
                    'errors' => $request->getAttribute('errors'),
                ]);
        }

        $nodeId = $request->getAttribute('id');
        $data = $this->getCacheService()
            ->deleteNode($nodeId)
            ->getAll()
            ->toArray();

        return $response->withJson([
            'data' => $data,
        ]);
    }

    /**
     * Save all changes from cache into DB.
     *
     * @param Request  $request
     * @param Response $response
     * @param          $args
     *
     * @throws \RuntimeException
     * @return Response
     */
    public function save(Request $request, Response $response, $args)
    {
        try {
            $data = $this->getCacheService()
                ->saveAll()
                ->getAll();
        } catch (\InvalidArgumentException $e) {
            return $response->withStatus(400)
                ->withJson([
                    'error' => $e->getMessage(),
                ]);
        }

        return $response->withJson([
            'data' => $data->toArray(),
        ]);
    }

    /**
     * @return CacheService
     */
    private function getCacheService(): CacheService
    {
        return new CacheService($this->cache, $this->connection);
    }
}
