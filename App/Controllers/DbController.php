<?php

namespace App\Controllers;

use App\Components\DB\Interfaces\DBInterface;
use App\Components\Tree;
use App\Models\NodeRepository;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class DbController
 *
 * @package App\Controllers
 */
class DbController
{
    /**
     * @var DBInterface
     */
    private $connection;
    /**
     * Default values of DB.
     *
     * @var array
     */
    private $defaultData = [
        [
            'id' => 1,
            'value' => 'Node 1',
            'parentId' => null,
            'isDeleted' => false,
        ],
        [
            'id' => 2,
            'value' => 'Node 2',
            'parentId' => 1,
            'isDeleted' => false,
        ],
        [
            'id' => 3,
            'value' => 'Node 3',
            'parentId' => 1,
            'isDeleted' => false,
        ],
        [
            'id' => 4,
            'value' => 'Node 4',
            'parentId' => 1,
            'isDeleted' => false,
        ],
        [
            'id' => 5,
            'value' => 'Node 5',
            'parentId' => 4,
            'isDeleted' => false,
        ],
        [
            'id' => 6,
            'value' => 'Node 6',
            'parentId' => 4,
            'isDeleted' => false,
        ],
        [
            'id' => 7,
            'value' => 'Node 7',
            'parentId' => 4,
            'isDeleted' => false,
        ],
        [
            'id' => 8,
            'value' => 'Node 8',
            'parentId' => 4,
            'isDeleted' => false,
        ],
        [
            'id' => 9,
            'value' => 'Node 9',
            'parentId' => 8,
            'isDeleted' => false,
        ],
        [
            'id' => 10,
            'value' => 'Node 10',
            'parentId' => 8,
            'isDeleted' => false,
        ],
        [
            'id' => 11,
            'value' => 'Node 11',
            'parentId' => 8,
            'isDeleted' => false,
        ],
        [
            'id' => 12,
            'value' => 'Node 12',
            'parentId' => 8,
            'isDeleted' => false,
        ],
        [
            'id' => 13,
            'value' => 'Node 13',
            'parentId' => 12,
            'isDeleted' => false,
        ],
        [
            'id' => 14,
            'value' => 'Node 14',
            'parentId' => 12,
            'isDeleted' => false,
        ],
        [
            'id' => 15,
            'value' => 'Node 15',
            'parentId' => 12,
            'isDeleted' => false,
        ],
        [
            'id' => 16,
            'value' => 'Node 16',
            'parentId' => 12,
            'isDeleted' => false,
        ],
        [
            'id' => 17,
            'value' => 'Node 17',
            'parentId' => 3,
            'isDeleted' => false,
        ],
        [
            'id' => 18,
            'value' => 'Node 18',
            'parentId' => 3,
            'isDeleted' => false,
        ],
        [
            'id' => 19,
            'value' => 'Node 19',
            'parentId' => 18,
            'isDeleted' => false,
        ],
        [
            'id' => 20,
            'value' => 'Node 20',
            'parentId' => 18,
            'isDeleted' => false,
        ],
        [
            'id' => 21,
            'value' => 'Node 21',
            'parentId' => 20,
            'isDeleted' => false,
        ],
        [
            'id' => 22,
            'value' => 'Node 22',
            'parentId' => 20,
            'isDeleted' => false,
        ],
        [
            'id' => 23,
            'value' => 'Node 23',
            'parentId' => 22,
            'isDeleted' => false,
        ],
        [
            'id' => 24,
            'value' => 'Node 24',
            'parentId' => 22,
            'isDeleted' => false,
        ],
        [
            'id' => 25,
            'value' => 'Node 25',
            'parentId' => 16,
            'isDeleted' => false,
        ],
        [
            'id' => 26,
            'value' => 'Node 26',
            'parentId' => 16,
            'isDeleted' => false,
        ],
        [
            'id' => 27,
            'value' => 'Node 27',
            'parentId' => 26,
            'isDeleted' => false,
        ],
        [
            'id' => 28,
            'value' => 'Node 28',
            'parentId' => 26,
            'isDeleted' => false,
        ],
        [
            'id' => 29,
            'value' => 'Node 29',
            'parentId' => 24,
            'isDeleted' => false,
        ],
        [
            'id' => 30,
            'value' => 'Node 30',
            'parentId' => 24,
            'isDeleted' => false,
        ],
    ];

    /**
     * CacheController constructor.
     *
     * @param DBInterface $connection
     */
    public function __construct(DBInterface $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Return all data from DB.
     *
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     */
    public function load(Request $request, Response $response, $args)
    {
        $data = $this->connection->loadAll();
        if (empty($data)) {
            try {
                $this->loadDefault();
            } catch (\InvalidArgumentException $e) {
                return $response->withStatus(400)->withJson([
                    'error' => $e->getMessage(),
                ]);
            }
        }

        return $response->withJson([
            'data' => $this->getTree()->toArray(),
        ]);
    }

    /**
     * Reset db to default.
     *
     * @param Request  $request
     * @param Response $response
     * @param          $args
     *
     * @return Response
     */
    public function reset(Request $request, Response $response, $args)
    {
        $this->connection->truncate();
        try {
            $this->loadDefault();
        } catch (\InvalidArgumentException $e) {
            return $response->withStatus(400)->withJson([
                'error' => $e->getMessage(),
            ]);
        }

        return $response->withJson([
            'data' => $this->getTree()->toArray(),
        ]);
    }

    /**
     * Build tree from all DB data.
     *
     * @return Tree
     */
    private function getTree(): Tree
    {
        $nodeRepository = new NodeRepository($this->connection);
        $tree = new Tree();
        foreach ($nodeRepository->loadAll() as $node) {
            $tree->addNode($node);
        }

        return $tree;
    }

    /**
     * Load default values into DB.
     *
     * @return void
     */
    private function loadDefault(): void
    {
        foreach ($this->defaultData as $item) {
            $this->connection->insert($item);
        }
    }
}