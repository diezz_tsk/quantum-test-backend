<?php

namespace App\Components\Cache;

interface CacheInterface
{
    public function get($key, $default = null);

    public function set($key, $value, $ttl = null);
}
