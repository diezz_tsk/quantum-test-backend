<?php

namespace App\Components\Cache;

class FileCache implements CacheInterface
{
    private $file;
    private $cache = [];

    public function __construct(string $file)
    {
        $this->file = $file;
    }

    public function get($key, $default = null)
    {
        $this->load();
        if (false === $this->has($key)) {
            return $default;
        }

        return $this->cache[$key];
    }

    public function set($key, $value, $ttl = null)
    {
        $this->cache[$key] = $value;
        $this->write();
    }

    public function has($key): bool
    {
        return array_key_exists($key, $this->cache);
    }

    private function load()
    {
        $this->checkFile();
        $content = file_get_contents($this->file);
        $this->cache = unserialize($content);
    }

    private function checkFile()
    {
        if (false === file_exists($this->file)) {
            file_put_contents($this->file, serialize([]));
        }
    }

    private function write()
    {
        $content = serialize($this->cache);
        file_put_contents($this->file, $content);
    }
}