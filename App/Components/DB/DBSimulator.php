<?php

namespace App\Components\DB;

use App\Components\DB\Interfaces\ConditionInterface;
use App\Components\DB\Interfaces\DBInterface;

/**
 * Class DBSimulator
 *
 * @package App
 */
class DBSimulator implements DBInterface
{
    /**
     * @var null|DBSimulator
     */
    private static $instance = null;
    /**
     * @var string
     */
    public $file;
    /**
     * @var
     */
    private $data;

    /**
     * Insert new row into DB. Return id of last inserted row.
     *
     * @param array $row array that will ne inserted.
     *
     * @return int
     */
    public function insert(array $row): int
    {
        $id = $row['id'] ?? $this->getNewId();
        $row['id'] = $id;
        $this->data[$id] = $row;
        $this->saveToFile();
        return $id;
    }

    /**
     * Generate new free id.
     *
     * @return int
     */
    public function getNewId(): int
    {
        if (empty($this->data)) {
            return 1;
        }

        return max(array_keys($this->data)) + 1;
    }

    /**
     * Update row.
     *
     * @param int   $id     row id.
     * @param array $values array of values to be updated in key-value term.
     *
     * @return void
     */
    public function update($id, array $values): void
    {
        if (false === $this->hasId($id)) {
            throw  new \InvalidArgumentException("Row with id: $id does't exist in DB. Use insert method");
        }

        $this->data[$id] = array_merge($this->data[$id], $values);
        $this->saveToFile();
    }

    /**
     * Delete row.
     *
     * @param int $id row id.
     *
     * @return void
     */
    public function delete(int $id): void
    {
        if (false === $this->hasId($id)) {
            throw  new \InvalidArgumentException("Row with id: $id does't exist in DB.");
        }

        unset($this->data[$id]);
        $this->saveToFile();
    }

    /**
     * Load all data from DB by condition.
     *
     * @param ConditionInterface|null $condition condition to filter result.
     *
     * @return array
     */
    public function loadAll(ConditionInterface $condition = null): array
    {
        if (null === $condition) {
            return $this->data;
        }

        return array_filter($this->data, function($item) use ($condition) {
            return $condition->compare($item);
        });
    }

    /**
     * Load one row from DB.
     *
     * @param int $id row id.
     *
     * @throws \InvalidArgumentException if row was not found.
     * @return array
     */
    public function loadOne(int $id): array
    {
        if (false === $this->hasId($id)) {
            throw  new \InvalidArgumentException("Row with id:$id does't exist in DB.");
        }

        return $this->data[$id];
    }

    /**
     * Truncate table
     *
     * @return void;
     */
    public function truncate(): void
    {
        $this->data = [];
        $this->saveToFile();
    }

    /**
     * Check that this table has row with id.
     *
     * @param int $id row id.
     *
     * @return bool
     */
    private function hasId(int $id): bool
    {
        return array_key_exists($id, $this->data);
    }

    /**
     * Save DB into file.
     *
     * @return void
     */
    private function saveToFile()
    {
        file_put_contents($this->file, json_encode($this->data));
    }

    /**
     * Load DB from file.
     *
     * @return array
     */
    private function readFromFile(): array
    {
        $rawJson = file_get_contents($this->file);
        return json_decode($rawJson, true);
    }

    /**
     * Destruct instance.
     *
     * @return void
     */
    public static function destruct()
    {
        static::$instance = null;
    }

    /**
     * Return DBSimulator instance.
     *
     * @param string $file file path to read/write db.
     *
     * @return DBSimulator
     */
    public static function getInstance(string $file)
    {
        if (null === static::$instance) {
            static::$instance = new static($file);
        }

        return static::$instance;
    }

    /**
     * DBSimulator constructor.
     *
     * @param string $file file path to read/write db.
     */
    private function __construct(string $file)
    {
        $this->file = $file;
        $this->data = $this->readFromFile();
    }
}
