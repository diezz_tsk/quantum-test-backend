<?php

namespace App\Components\DB\Conditions;

use App\Components\DB\Interfaces\ConditionInterface;

/**
 * Class EqualsCondition
 *
 * @package App\Components\DB
 */
class EqualsCondition implements ConditionInterface
{
    /**
     * Field of table to check.
     *
     * @var string
     */
    private $field;
    /**
     * Value to compare with table value.
     *
     * @var mixed
     */
    private $value;

    /**
     * EqualsCondition constructor.
     *
     * @param $field
     * @param $value
     */
    public function __construct($field, $value)
    {
        $this->field = $field;
        $this->value = $value;
    }

    /**
     * Verify that the table row satisfies the specified condition.
     *
     * @param array $row table row.
     *
     * @return bool
     */
    public function compare(array $row): bool
    {
        if (!array_key_exists($this->field, $row)) {
            throw new \InvalidArgumentException("There is no {$this->field} field in the table");
        }

        $dbValue = $row[$this->field];
        return $dbValue == $this->value;
    }
}