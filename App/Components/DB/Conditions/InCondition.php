<?php

namespace App\Components\DB\Conditions;

use App\Components\DB\Interfaces\ConditionInterface;

/**
 * Class EqualsCondition
 *
 * @package App\Components\DB
 */
class InCondition implements ConditionInterface
{
    /**
     * Field of table to check.
     *
     * @var string
     */
    private $field;
    /**
     * Array of values to be compared with field.
     *
     * @var array
     */
    private $values = [];

    /**
     * EqualsCondition constructor.
     *
     * @param       $field
     * @param array $value
     */
    public function __construct($field, array $values)
    {
        $this->field = $field;
        $this->values = $values;
    }

    /**
     * Verify that the table row satisfies the specified condition.
     *
     * @param array $row table row.
     *
     * @return bool
     */
    public function compare(array $row): bool
    {
        if (!array_key_exists($this->field, $row)) {
            throw new \InvalidArgumentException("There is no {$this->field} field in the table");
        }

        $dbValue = $row[$this->field];
        return in_array($dbValue, $this->values);
    }
}