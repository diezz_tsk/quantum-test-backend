<?php

namespace App\Components\DB;

use App\Components\DB\Interfaces\ConditionInterface;
use App\Components\DB\Interfaces\DBInterface;

/**
 * Class Repository
 *
 * @package App\Components\DB
 */
abstract class Repository
{
    /**
     * DB Connection instance.
     *
     * @var DBInterface
     */
    protected $db;

    /**
     * Repository constructor.
     *
     * @param $db
     */
    public function __construct(DBInterface $db)
    {
        $this->db = $db;
    }

    /**
     * Return class name of entity that the repo will create.
     *
     * @return string
     */
    abstract public function getModelClassName(): string;

    /**
     * Load in row from db.
     *
     * @param int $id
     *
     * @throws \InvalidArgumentException if row was not found.
     * @return Model
     */
    public function loadOne($id): Model
    {
        $data = $this->db->loadOne($id);
        return $this->createModel($data);
    }

    /**
     * Load all rows from DB. Return array of models.
     *
     * @return Model[]
     */
    public function loadAll(ConditionInterface $condition = null): array
    {
        $result = [];
        $data = $this->db->loadAll($condition);
        foreach ($data as $rawData) {
            $result[] = $this->createModel($rawData);
        }

        return $result;
    }

    /**
     * Delete row associated with model.
     *
     * @param Model $model
     *
     * @return void
     */
    public function delete(Model $model)
    {
        $this->db->delete($model->id);
        unset($model);
    }

    /**
     * Save model changes into DB.
     *
     * @param Model $model
     *
     * @return void
     */
    public function save(Model $model)
    {
        if (true === $model->isNew()) {
            $this->insert($model);
        } else {
            $this->update($model);
        }
    }

    /**
     * Insert row into DB.
     *
     * @param Model $model
     *
     * @return void
     */
    protected function insert(Model $model)
    {
        $lastId = $this->db->insert($model->toArray(false));
        $model->setId($lastId);
        $model->setNotNew();
        $model->resetTouched();
    }

    /**
     * Update current model. Write to db only changed attributes.
     *
     * @param Model $model model to be updated.
     *
     * @return void
     */
    protected function update(Model $model)
    {
        $data = [];
        foreach ($model->getTouched() as $field) {
            $data[$field] = $model->{$field};
        }
        $this->db->update($model->id, $data);
        $model->resetTouched();
    }

    /**
     * Create new Model instance related with the repository.
     *
     * @param array $rawData
     *
     * @return Model
     */
    protected function createModel(array $rawData)
    {
        $modelClassName = $this->getModelClassName();
        return new $modelClassName($rawData, false);
    }
}
