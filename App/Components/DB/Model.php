<?php

namespace App\Components\DB;

use App\Components\Db\Exceptions\UnknownPropertyException;

/**
 * Class Model
 *
 * @property int $id
 * @package App
 */
abstract class Model
{
    /**
     * Default attributes of the model.
     *
     * @var array
     */
    protected $attributes = [
        'id' => null,
    ];
    /**
     * Array of fields that as touched.
     *
     * @var array
     */
    protected $touched = [];
    /**
     * Is the model newly created.
     *
     * @var bool
     */
    protected $isNew = false;

    /**
     * Model constructor.
     *
     * @param array $attributes
     * @param bool  $isNew
     */
    public function __construct($attributes = [], $isNew = true)
    {
        $this->init();
        $this->isNew = $isNew;
        $this->setAttributes($attributes);
    }

    /**
     * Init the model by default attributes.
     *
     * @return void
     */
    public function init(): void
    {
        foreach ($this->getFields() as $fieldName) {
            $this->attributes[$fieldName] = null;
        }
    }

    /**
     * Must return an array of field name that support this model.
     *
     * @return array
     */
    abstract public function getFields(): array;

    /**
     * Return an array of fields that support the model.
     *
     * @return array
     */
    protected function fields(): array
    {
        return array_merge(['id'], $this->getFields());
    }

    /**
     * Set all attributes values at once.
     *
     * @param array $attributes array of attributes in key-value terms.
     *
     * @throws UnknownPropertyException
     * @return void
     */
    public function setAttributes(array $attributes): void
    {
        foreach ($attributes as $fieldName => $fieldValue) {
            $this->setAttribute($fieldName, $fieldValue);
        }
    }

    /**
     * Set value for the model attribute.
     *
     * @param string $fieldName  attribute(field) name.
     * @param mixed  $fieldValue value.
     *
     * @throws UnknownPropertyException
     * @return void
     */
    protected function setAttribute($fieldName, $fieldValue)
    {
        if (false === $this->hasFields($fieldName)) {
            throw new UnknownPropertyException("You tried to set unknown property $fieldName");
        }

        $this->attributes[$fieldName] = $fieldValue;
    }

    /**
     * Return value of attribute(field).
     *
     * @param string $fieldName
     *
     * @throws UnknownPropertyException
     * @return mixed
     */
    protected function getAttribute($fieldName)
    {
        if (true === $this->hasFields($fieldName)) {
            return $this->attributes[$fieldName];
        }

        throw new UnknownPropertyException("You tried to get unknown property $fieldName");
    }

    /**
     * Try to set attribute value by magic method.
     *
     * @param string $name  field name.
     * @param mixed  $value value to be set.
     *
     * @throws UnknownPropertyException
     */
    public function __set($name, $value)
    {
        $this->setAttribute($name, $value);
        if (false === $this->isNew() && false === in_array($name, $this->touched)) {
            $this->touched[] = $name;
        }
    }

    /**
     * Try to get attribute value.
     *
     * @param string $name attribute name.
     *
     * @throws UnknownPropertyException
     * @return mixed
     */
    public function __get($name)
    {
        return $this->getAttribute($name);
    }

    /**
     * Check the model has field.
     *
     * @param string $fieldName
     *
     * @return bool
     */
    protected function hasFields($fieldName): bool
    {
        $fields = array_merge(['id'], $this->getFields());
        return in_array($fieldName, $fields, true);
    }

    /**
     * Mark the model as not new.
     *
     * @return void
     */
    public function setNotNew()
    {
        $this->isNew = false;
    }

    /**
     * Set id for the model.
     *
     * @param int $id
     *
     * @return void
     */
    public function setId($id)
    {
        $this->attributes['id'] = $id;
    }

    /**
     * Check that model is new.
     *
     * @return bool
     */
    public function isNew(): bool
    {
        return $this->isNew;
    }

    /**
     * Return list of touched fields.
     *
     * @return array
     */
    public function getTouched(): array
    {
        return $this->touched;
    }

    /**
     * Mark all fields as untouched.
     *
     * @return void
     */
    public function resetTouched()
    {
        $this->touched = [];
    }

    /**
     * Check the model has touched fields.
     *
     * @return bool
     */
    public function isTouched(): bool
    {
        return !empty($this->touched);
    }

    /**
     * Return array representation of the model.
     *
     * @param bool $hasTouched
     *
     * @return array
     */
    public function toArray($hasTouched = true): array
    {
        $result = [];
        foreach ($this->fields() as $field) {
            $result[$field] = $this->getAttribute($field);
        }
        if (true === $hasTouched) {
            $result['isTouched'] = $this->isNew() || $this->isTouched();
        }

        return $result;
    }
}
