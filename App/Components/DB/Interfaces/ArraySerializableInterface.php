<?php

namespace App\Components\DB\Interfaces;

/**
 * Interface ArraySerializableInterface
 *
 * @package App\Components\DB\Interfaces
 */
interface ArraySerializableInterface
{
    /**
     * Convert instance into array.
     *
     * @return array
     */
    public function toArray(): array;
}
