<?php

namespace App\Components\DB\Interfaces;

interface DBInterface
{

    /**
     * Insert new row into DB. Return id of last inserted row.
     *
     * @param array $row array that will ne inserted.
     *
     * @return int
     */
    public function insert(array $row): int;

    /**
     * Update row.
     *
     * @param int   $id     row id.
     * @param array $values array of values to be updated in key-value term.
     *
     * @return void
     */
    public function update($id, array $values): void;

    /**
     * Delete row.
     *
     * @param int $id row id.
     *
     * @return void
     */
    public function delete(int $id): void;

    /**
     * Load all data from DB by condition.
     *
     * @param ConditionInterface|null $condition condition to filter result.
     *
     * @return array
     */
    public function loadAll(ConditionInterface $condition = null): array;

    /**
     * Load one row from DB.
     *
     * @param int $id row id.
     *
     * @throws \InvalidArgumentException if row was not found.
     * @return array
     */
    public function loadOne(int $id): array;

    /**
     * Truncate table
     *
     * @return void;
     */
    public function truncate(): void;

    /**
     * Generate new id.
     *
     * @return int
     */
    public function getNewId(): int;
}