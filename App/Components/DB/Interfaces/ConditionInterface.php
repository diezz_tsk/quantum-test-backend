<?php

namespace App\Components\DB\Interfaces;

/**
 * Interface ConditionInterface
 *
 * @package App\Components\DB\Interfaces
 */
interface ConditionInterface
{
    /**
     * Verify that the table row satisfies the specified condition.
     *
     * @param array $row table row.
     *
     * @return bool
     */
    public function compare(array $row): bool;
}
