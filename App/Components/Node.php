<?php

namespace App\Components;

use App\Models\Interfaces\NodeModelInterface;
use App\Models\NodeModel;

/**
 * Class Node
 *
 * @package App
 */
class Node
{
    /**
     * @var NodeModel
     */
    private $value;
    /**
     * @var Node[]
     */
    private $children = [];

    /**
     * Node constructor.
     *
     * @param NodeModelInterface $node
     */
    public function __construct(NodeModel $node)
    {
        $this->value = $node;
    }

    /**
     * Set all children nodes at once.
     *
     * @param Node[] $children list of children nodes.
     *
     * @return Node
     */
    public function setChildren(array $children): Node
    {
        $this->children = [];
        foreach ($children as $child) {
            $this->addChild($child);
        }

        return $this;
    }

    /**
     * Add child node.
     *
     * @param Node $node child node to be added.
     *
     * @return Node
     */
    public function addChild(Node $node): Node
    {
        $this->children[] = $node;
        return $this;
    }

    /**
     * Return NodeModel instance.
     *
     * @return NodeModel
     */
    public function getValue(): NodeModel
    {
        return $this->value;
    }

    /**
     * Set NodeModel instance as node value.
     *
     * @param NodeModel $value
     *
     * @return Node
     */
    public function setValue(NodeModel $value): Node
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Find node by its id.
     *
     * @param int $nodeId node id.
     *
     * @return Node|null
     */
    public function findNode(int $nodeId):?Node
    {
        if ($nodeId === $this->getValue()->getID()) {
            return $this;
        }

        foreach ($this->children as $child) {
            if ($nodeId === $child->getValue()->getId()) {
                return $child;
            }
            if (true === $child->isLeaf()) {
                continue;
            }

            $result = $child->findNode($nodeId);
            if (null !== $result) {
                return $result;
            }
        }

        return null;
    }

    /**
     * Check whether this node is deleted.
     *
     * @return bool
     */
    public function isLeaf(): bool
    {
        return count($this->children) === 0;
    }

    /**
     * Delete the node.
     *
     * @return Node
     */
    public function delete()
    {
        $this->getValue()->isDeleted = true;
        foreach ($this->children as $child) {
            $child->delete();
        }

        return $this;
    }

    /**
     * Return id of the node.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->getValue()->getId();
    }

    /**
     * Check that node is deleted.
     *
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->getValue()->isDeleted;
    }

    /**
     * Return count of children nodes.
     *
     * @return int
     */
    public function getChildrenCount(): int
    {
        return count($this->children);
    }

    /**
     * Check whether this node has a children.
     *
     * @return bool
     */
    public function hasChildren(): bool
    {
        return $this->getChildrenCount() > 0;
    }

    /**
     * Return list of all children nodes as flat array.
     *
     * @return Node[]
     */
    public function getAllChildren(): array
    {
        $result = [];
        foreach ($this->children as $child) {
            $result[] = $child;
            if (true === $child->hasChildren()) {
                $result = array_merge($result, $child->getAllChildren());
            }
        }

        return $result;
    }

    /**
     * Return tree representation as array.
     *
     * @return array
     */
    public function toArray(): array
    {
        $children = [];
        foreach ($this->children as $child) {
            $children[] = $child->toArray();
        }

        return array_merge($this->getValue()->toArray(), [
            'children' => $children,
        ]);
    }
}
