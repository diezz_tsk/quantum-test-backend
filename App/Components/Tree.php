<?php

namespace App\Components;

/**
 * Class Tree
 *
 * @package App
 */

use App\Components\DB\Interfaces\ArraySerializableInterface;
use App\Models\Interfaces\NodeModelInterface;

/**
 * Class Tree
 *
 * @package App
 */
class Tree implements \Countable, ArraySerializableInterface
{
    /**
     * @var Node[]
     */
    private $nodeList = [];

    /**
     * Count elements of an object
     *
     * @link  http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     * @since 5.1.0
     */
    public function count()
    {
        return count($this->nodeList);
    }

    /**
     * Add node into tree.
     *
     * @param NodeModelInterface $model
     *
     * @return $this
     */
    public function addNode(NodeModelInterface $model)
    {
        $id = $model->getId();
        if (true === $this->hasNode($id)) {
            return $this;
        }
        $node = new Node($model);
        $this->addToList($node);
        $this->checkForChildren($node);

        return $this;
    }

    /**
     * Remove node from tree.
     *
     * @param int $nodeId
     *
     * @throws NodeNotFoundException
     * @return void
     */
    public function removeNode(int $nodeId): void
    {
        $node = $this->findNode($nodeId);
        if (null === $node) {
            throw new NodeNotFoundException("Unable to find node: $nodeId");
        }

        $node->delete();
    }

    /**
     * Check that the tree have children of added node.
     *
     * @param Node $node
     *
     * @return void
     */
    private function checkForChildren(Node $node): void
    {
        foreach ($this->nodeList as $index => $existNode) {
            $condition = $existNode->getValue()->getParentId() !== $node->getValue()->getId();
            if (true === $condition) {
                continue;
            }
            $node->addChild($existNode);
            $this->checkIsDeleted($node, $existNode);
            unset($this->nodeList[$index]);
        }
    }

    /**
     * Add node to tree.
     *
     * @param Node $node
     *
     * @return void
     */
    private function addToList(Node $node)
    {
        $parent = $this->findNode($node->getValue()->getParentId());
        if (null !== $parent) {
            $parent->addChild($node);
            $this->checkIsDeleted($parent, $node);
        } else {
            $this->nodeList[] = $node;
        }
    }

    /**
     * Check that parent node is deleted and mark deleted new node if need.
     *
     * @param Node $parent parent node.
     * @param Node $node   new node.
     *
     * @return void
     */
    private function checkIsDeleted(Node $parent, Node $node): void
    {
        if (true === $parent->isDeleted() && false === $node->isDeleted()) {
            $node->delete();
        }
    }

    /**
     * Check that tree already has node.
     *
     * @param int|null $nodeId
     *
     * @return bool
     */
    private function hasNode(?int $nodeId): bool
    {
        $node = $this->findNode($nodeId);
        return null !== $node;
    }

    /**
     * Find child node by id.
     *
     * @param int|null $nodeId
     *
     * @return Node|null
     */
    public function findNode(?int $nodeId):?Node
    {
        if (null === $nodeId) {
            return null;
        }

        foreach ($this->nodeList as $node) {
            $result = $node->findNode($nodeId);
            if (null !== $result) {
                return $result;
            }
        }

        return null;
    }

    /**
     * Return tree as flat array. All children will be in one level.
     *
     * @return Node[]
     */
    public function asFlatArray(): array
    {
        $result = [];
        foreach ($this->nodeList as $node) {
            $result[] = $node;
            $result = array_merge($result, $node->getAllChildren());
        }

        return $result;
    }

    /**
     * Return tree converted into array.
     *
     * @return array
     */
    public function toArray(): array
    {
        $result = [];
        foreach ($this->nodeList as $node) {
            $result[] = $node->toArray();
        }

        return $result;
    }
}
