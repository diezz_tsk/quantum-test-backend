<?php

namespace App\Services;

use App\Components\DB\Conditions\InCondition;
use App\Components\Node;
use App\Components\Tree;
use App\Models\NodeModel;
use App\Models\NodeRepository;
use App\Components\Cache\CacheInterface;
use App\Components\DB\Interfaces\DBInterface;
use App\Components\DB\Interfaces\ArraySerializableInterface;

/**
 * Class CacheService
 *
 * @package App\Services
 */
class CacheService
{
    /**
     * Cache key to store tree structure.
     */
    CONST CACHE_TREE_KEY = 'app.cache.tree';
    /**
     * Cache key to store last inserted id.
     */
    CONST LAST_INSERTED_ID = 'app.cache.last.inserted.id';

    /**
     * @var CacheInterface
     */
    private $cache;
    /**
     * @var DBInterface
     */
    private $connection;

    /**
     * CacheService constructor.
     *
     * @param CacheInterface $cache
     * @param DBInterface    $connection
     */
    public function __construct(CacheInterface $cache, DBInterface $connection)
    {
        $this->cache = $cache;
        $this->connection = $connection;
    }

    /**
     * @return ArraySerializableInterface
     */
    public function getAll(): ArraySerializableInterface
    {
        return $this->loadTree();
    }

    /**
     * @param int $nodeId
     *
     * @return NodeModel|null
     */
    public function getNode(int $nodeId):?NodeModel
    {
        $node = $this->loadTree()
            ->findNode($nodeId);
        if (null === $node) {
            return null;
        }

        return $node->getValue();
    }

    /**
     * @return CacheService
     */
    public function reset()
    {
        $this->saveTree(new Tree());
        return $this;
    }

    /**
     * @param int $nodeId
     *
     * @throws \InvalidArgumentException if row was not found.
     * @return CacheService
     */
    public function loadNode(int $nodeId)
    {
        /** @var NodeModel $node */
        $node = $this->getNodeRepository()
            ->loadOne($nodeId);
        $tree = $this->loadTree();
        $tree->addNode($node);
        $this->saveTree($tree);
        return $this;
    }

    /**
     * @param string $value
     * @param int    $parentId
     *
     * @return int
     */
    public function addNewNode(string $value, int $parentId): int
    {
        $id = $this->getNewId();
        $tree = $this->loadTree();
        $tree->addNode(new NodeModel([
            'id' => $id,
            'value' => $value,
            'parentId' => $parentId,
            'isDeleted' => false,
        ]));
        $this->saveTree($tree);
        $this->cache->set(self::LAST_INSERTED_ID, $id);
        return $id;
    }

    /**
     * @param int $nodeId
     *
     * @return CacheService
     */
    public function deleteNode(int $nodeId)
    {
        $tree = $this->loadTree();
        $tree->removeNode($nodeId);
        $this->saveTree($tree);
        return $this;
    }

    /**
     * @throws \InvalidArgumentException
     * @return CacheService
     */
    public function saveAll()
    {
        $tree = $this->loadTree();
        $this->getNodeRepository()
            ->saveTree($tree);
        $this->cache->set(self::LAST_INSERTED_ID, $this->connection->getNewId() - 1);
        $this->saveTree($this->reloadTree($tree));
        return $this;
    }

    /**
     * Reload all tree nodes from DB.
     *
     * @param Tree $tree
     *
     * @return Tree
     */
    private function reloadTree(Tree $tree)
    {
        $ids = array_map(function(Node $node) {
            return $node->getId();
        }, $tree->asFlatArray());
        $condition = new InCondition('id', $ids);
        /** @var NodeModel[] $nodeList */
        $nodeList = $this->getNodeRepository()
            ->loadAll($condition);
        $reloadedTree = new Tree();
        usort($nodeList, function(NodeModel $right, NodeModel $left) use ($ids) {
            $order = array_flip($ids);
            return $order[$left->getId()] < $order[$right->getId()];
        });
        foreach ($nodeList as $node) {
            $reloadedTree->addNode($node);
        }
        return $reloadedTree;
    }

    /**
     * Return NodeRepository instance.
     *
     * @return NodeRepository
     */
    private function getNodeRepository(): NodeRepository
    {
        return new NodeRepository($this->connection);
    }

    /**
     * Load tree from cache.
     *
     * @return Tree
     */
    private function loadTree(): Tree
    {
        $default = new Tree();
        return $this->cache->get(self::CACHE_TREE_KEY, $default);
    }

    /**
     * Save tree into cache.
     *
     * @param Tree $tree
     *
     * @return void
     */
    private function saveTree(Tree $tree): void
    {
        $this->cache->set(self::CACHE_TREE_KEY, $tree);
    }

    /**
     * Return new available id.
     *
     * @return int
     */
    private function getNewId(): int
    {
        return (int)$this->cache->get(self::LAST_INSERTED_ID, $this->connection->getNewId() - 1) + 1;
    }
}
