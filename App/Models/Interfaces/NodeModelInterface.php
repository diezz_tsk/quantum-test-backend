<?php

namespace App\Models\Interfaces;

/**
 * Interface NodeModelInterface
 *
 * @package App
 */
interface NodeModelInterface
{
    /**
     * Return model id.
     *
     * @return int
     */
    public function getId(): ?int;

    /**
     * Return parent model id.
     *
     * @return int|null
     */
    public function getParentId():?int;

    /**
     * Return value of model.
     *
     * @return string
     */
    public function getValue(): string;
}