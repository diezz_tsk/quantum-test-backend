<?php

namespace App\Models;

use App\Components\DB\Model;
use App\Models\Interfaces\NodeModelInterface;

/**
 * Class NodeModel
 *
 * @property int    $parentId
 * @property string $value
 * @property bool   $isDeleted
 *
 * @package App\DB
 */
class NodeModel extends Model implements NodeModelInterface
{
    /**
     * Return an array of field name that support this model.
     *
     * @return array
     */
    public function getFields(): array
    {
        return [
            'value',
            'parentId',
            'isDeleted',
        ];
    }

    /**
     * Return model id.
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Return parent model id.
     *
     * @return int|null
     */
    public function getParentId():?int
    {
        return $this->parentId;
    }

    /**
     * Return value of model.
     *
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}
