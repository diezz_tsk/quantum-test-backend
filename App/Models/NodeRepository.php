<?php

namespace App\Models;

use App\Components\DB\Conditions\EqualsCondition;
use App\Components\DB\Repository;
use App\Components\Node;
use App\Components\Tree;

class NodeRepository extends Repository
{
    /**
     * Return class name of entity that the repo will create.
     *
     * @return string
     */
    public function getModelClassName(): string
    {
        return NodeModel::class;
    }

    /**
     * Save all changes of tree into DB at once.
     *
     * @param Tree $tree
     *
     * @throws \InvalidArgumentException
     * @return void
     */
    public function saveTree(Tree $tree): void
    {
        $nodes = $tree->asFlatArray();
        $updated = array_filter($nodes, function(Node $node) {
            return $node->getValue()->isTouched();
        });
        $deleted = array_filter($nodes, function(Node $node) {
            return $node->isDeleted();
        });
        $inserted = array_filter($nodes, function(Node $node) {
            return $node->getValue()->isNew();
        });
        $this->updateNodes($updated);
        $this->insertNodes($inserted);
        $this->deleteNodes($deleted);
    }

    /**
     * Update nodes.
     *
     * @param Node[] $updated
     *
     * @return void
     */
    private function updateNodes(array $updated)
    {
        foreach ($updated as $row) {
            $this->update($row->getValue());
        }
    }

    /**
     * Insert new nodes into db.
     *
     * @param Node[] $inserted
     *
     * @return void
     */
    private function insertNodes(array $inserted)
    {
        foreach ($inserted as $row) {
            $this->insert($row->getValue());
        }
    }

    /**
     * Delete nodes.
     *
     * @param  Node[] $deleted
     *
     * @return void
     */
    private function deleteNodes(array $deleted)
    {
        $deleted = $this->findAllChild(array_map(function(Node $node) {
            return $node->getValue();
        }, $deleted));
        foreach ($deleted as $row) {
            $row->isDeleted = true;
            $this->update($row);
        }
    }

    /**
     * Find all children for deleted nodes.
     *
     * @param NodeModel[] $nodes
     *
     * @return NodeModel[]
     */
    private function findAllChild(array $nodes)
    {
        $result = [];
        foreach ($nodes as $node) {
            $result[] = $node;
            $condition = new EqualsCondition('parentId', $node->getId());
            $children = $this->loadAll($condition);
            if (!empty($children)) {
                $children = array_merge($children, $this->findAllChild($children));
            }
            $result = array_merge($result, $children);
        }

        return array_map("unserialize", array_unique(array_map("serialize", $result)));
    }
}
