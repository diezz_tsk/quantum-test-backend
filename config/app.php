<?php

$runtime = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'runtime';

return [
    'settings' => [
        'displayErrorDetails' => env('DEBUG_MODE', false),
        'allow-origin' => env('ORIGIN'),
        'cache' => [
            'cacheFile' => $runtime . '/cache/cache',
        ],
        'db' => [
            'file' => $runtime . '/db.json',
        ],
        'determineRouteBeforeAppMiddleware' => true,
    ],
];