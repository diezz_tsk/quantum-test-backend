<?php

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../App/helpers.php';

(new Dotenv\Dotenv(dirname(__DIR__)))->load();
$config = require_once __DIR__ . '/../config/app.php';
$app = new Slim\App($config);
require_once __DIR__ . '/../App/routes.php';
$app->run();
